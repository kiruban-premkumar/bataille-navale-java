package ocean;

import geometrie.Coord;

import java.util.ArrayList;


public class Case {
	
	private Coord coordonnees;
	private ArrayList<Bateau> bateauxStationnes; 
	
	public Case(Coord c) {
		this.coordonnees = c;
		this.bateauxStationnes = new ArrayList<Bateau>();
	}
	
	public Coord getCoordonnees() {
		return coordonnees;
	}
	
	public void addBateau(Bateau bateau) {
		bateauxStationnes.add(bateau);
	}
	
	public void removeBateau(Bateau bateau) {
		for(int i=0; i < bateauxStationnes.size(); i++) {
			if (bateauxStationnes.get(i).getNom() == bateau.getNom())
				bateauxStationnes.remove(i);
		}
	}

}
