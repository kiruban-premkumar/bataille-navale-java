package ocean;

import geometrie.Coord;

import java.util.ArrayList;


public interface Bateau {
	void seDeplacer(Ocean ocean);
	void action(ArrayList<Bateau> ListeBateaux);
	Coord getCoordonnee();
	String getNom();
	int getCapacite();
	void setCapacite();
	void restoreVie();
	boolean estBateauHopital();
	boolean canAttack();
	int getVie();
	void changeCoord(Ocean ocean);
	void restore(Ocean ocean);
	int getId();
}
