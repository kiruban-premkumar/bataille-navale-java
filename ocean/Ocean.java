package ocean;

import exception.BateauException;
import exception.OceanException;
import geometrie.Coord;

import java.util.ArrayList;


public class Ocean {
	
	private Case[][] matrice;
	private ArrayList<Bateau> bateaux;
	private int nbL, nbC;
	
	public Ocean(int nbLignes, int nbColonnes) throws OceanException {
		if (nbLignes < 0 || nbColonnes < 0)
			throw new OceanException(nbLignes,nbColonnes);
		else {
			this.matrice = new Case[nbLignes][nbColonnes];
			this.bateaux = new ArrayList<Bateau>();
			this.nbL = nbLignes;
			this.nbC = nbColonnes;
			
			for (int x = 0; x < nbL; ++x) {
				for (int y = 0; y < nbC; ++y) {
					matrice[x][y] = new Case(new Coord(x,y));
				}
			}
		}
	}
	
	public Case[][] getMatrice() {
		return matrice;
	}
	
	public ArrayList<Bateau> getListeBateaux() {
		return bateaux;
	}
	
	public int getLigne() {
		return nbL;
	}
	
	public int getColonne() {
		return nbC;
	}
	
	public int getBateaux() {
		int nb=0, id=0;
		for (int i = 0; i < bateaux.size(); i++) {
			if (!bateaux.get(i).estBateauHopital()) {
				nb += 1;
				id = i;
			}
		}
		if (nb == 1)
			return id;
		return -1;
	}

	public void setBateau(Bateau bateau) throws BateauException {
		if (bateau.getCoordonnee().getLigne() >= nbL || bateau.getCoordonnee().getColonne() >= nbC 
				|| bateau.getCoordonnee().getLigne() < 0 || bateau.getCoordonnee().getColonne() < 0)
			throw new BateauException();
		else {
			bateaux.add(bateau);
			matrice[bateau.getCoordonnee().getLigne()][bateau.getCoordonnee().getColonne()].addBateau(bateau);
		}
	}
	
	public boolean autodestruction(int i) {
		return (bateaux.get(i).getCapacite() == 0);
	}
	
	public void destruction(int i) {
		matrice[bateaux.get(i).getCoordonnee().getLigne()][bateaux.get(i).getCoordonnee().getColonne()].
		removeBateau(bateaux.get(i));
		bateaux.remove(i);
	}

}
