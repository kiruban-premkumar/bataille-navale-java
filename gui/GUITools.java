package gui;

import jPanelImageBg.JPanelImageBg;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MediaTracker;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;


class Element extends ImageIcon {
  private static final long serialVersionUID = 1L;
  private int id, x, y;

  public Element(String nomFichier, int id, int x, int y) {
    super(nomFichier);
    this.id = id;
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }

  public int getId() {
    return id;
  }
}

class JCanvas extends JPanel {
  private static final long serialVersionUID = 1L;
  private static final int NON_TROUVE = -1;
  private List<Element> images = new LinkedList<Element>();

  public boolean ajouterImage(Element e) {
    if (e.getImageLoadStatus() == MediaTracker.ERRORED)
      return false;
    if (chercher(e.getId()) != NON_TROUVE)
      return false;
    synchronized (images) {
      images.add(e);
    }
    repaint();
    return true;
  }

  public boolean retirerImage(int id) {
    int index = chercher(id);
    if (index != NON_TROUVE) {
      synchronized (images) {
        images.remove(index);
      }
      repaint();
      return true;
    }
    return false;
  }

  public boolean placerImage(int id, int x, int y) {
    int index = chercher(id);
    if (index != NON_TROUVE) {
      synchronized (images) {
        Element e = images.get(index);
        e.setX(x);
        e.setY(y);
      }
      repaint();
      return true;
    }
    return false;
  }

  private int chercher(int id) {
    for (int i = 0; i < images.size(); ++i)
      if (images.get(i).getId() == id)
        return i;
    return NON_TROUVE;
  }

  public void effacer() {
    synchronized (images) {
      images.clear();
    }
    repaint();
  }

  @Override
  public void paint(Graphics g) {
    super.paint(g);
    synchronized (images) {
      for (Iterator<Element> iter = images.iterator(); iter.hasNext();) {
        Graphics2D g2d = (Graphics2D) g;
        Element e = iter.next();
        g2d.drawImage(e.getImage(), e.getX(), e.getY(), null);
      }
    }
  }
  
}

/** Classe proposant un ensemble de m�thodes statiques permettant 
 * l'affichage d'images au sein d'une fen�tre.
 * <p>
 * L'ouverture de la fen�tre ({@link GUITools#ouvrir}) provoque son 
 * affichage. Tout ajout d'image ({@link GUITools#ajouterImage}) provoque son 
 * affichage au sein de la fen�tre. De m�me, tout placement (m�thode
 * {@link GUITools#placerImage}) ou retrait ({@link GUITools#retirerImage} et {@link GUITools#retirerTout})
 * provoque la mise � jour de l'affichage.
 * <p>
 * Les images doivent �tre fournies sous la forme de fichier gif, jpeg
 * ou png. Les fichiers gif anim�s ou transparents sont support�s.
 * <p>
 * La fen�tre peut �tre iconifi�e, maximis�e ou ferm�e par les icones
 * se trouvant en haut � droite. Toutefois, la fermeture de la fen�tre 
 * par l'icone correspondant provoque automatiquement la fin de 
 * l'application (System.exit(0)).
 * <p>
 * Tout appel de m�thode avant l'ouverture de la fen�tre provoque 
 * une erreur. Enfin, si la m�thode {@link GUITools#fermer} a �t� appel�e, tout 
 * appel autre qu'� la m�thode {@link GUITools#ouvrir} provoque une erreur.
 * */
public class GUITools {
  private static JFrame frame = null;
  private static JCanvas panel = null;

  private GUITools() {}
  
  /** Ouvre la fen�tre dans laquelle seront affich�es les images
   * @param titre Intitul� de la fen�tre.
   * @param largeur Largeur initiale de la fen�tre (nombre de pixels) 
   * @param hauteur Hauteur initiale de la fen�tre (nombre de pixels)
   */
  public static void ouvrir(String titre, int largeur, int hauteur) { 
    frame = new JFrame(titre);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    //Color transparent = new Color (0, 0, 0, 0);

    try {
    	frame.setContentPane(new JPanelImageBg("Images/fond.png",JPanelImageBg.TEXTURE));
    	BufferedImage myImage = ImageIO.read(new File("Images/icon.png"));
    	frame.setIconImage(myImage);
    } catch (IOException e) { 
        e.printStackTrace(); 
    }
    
    panel = new JCanvas();
    panel.setBackground(Color.GRAY);
    panel.setOpaque(false);
    panel.setPreferredSize(new Dimension(largeur, hauteur));

    frame.getContentPane().add(panel);
    frame.pack();

    frame.setSize(largeur, hauteur);
    frame.setLocationRelativeTo(frame.getParent());
    frame.setVisible(true);
  }

  /** Ajoute une image qui sera affich�e dans la fen�tre.
   * @param id Identifiant de l'image
   * @param nomFichier Nom (relatif ou absolu) du fichier (gif, jpeg ou png) 
   * contenant l'image
   * @param x Position horizontale (0 � gauche de la fen�tre)
   * @param y Position verticale (0 en haut de la fen�tre)
   * @return true si l'image a pu �tre ajout�e (le fichier existe et 
   * est dans un format acceptable) et que la fen�tre ne contient auncune 
   * image portant un idenfiant �gal � id 
   */
  public static boolean ajouterImage(int id, String nomFichier, int x, int y) {
    return panel.ajouterImage(new Element(nomFichier, id, x, y));
  }
  
  /** Retire une image de la fen�tre.
   * @param id Identifiant de l'image devant �tre retir�e
   * @return false si aucune image de la fen�tre ne porte un idenfiant 
   * �gal � id 
   */
  public static boolean retirerImage(int id) {
    return panel.retirerImage(id);
  }

  /** Modifie le placement d'une image de la fen�tre.
   * @param id Identifiant de l'image devant �tre plac�e
   * @param x Position horizontale (0 � gauche de la fen�tre)
   * @param y Position verticale (0 en haut de la fen�tre)
   * @return false si aucune image de la fen�tre ne porte un idenfiant 
   * �gal � id 
   */
  public static boolean placerImage(int id, int x, int y) {
    return panel.placerImage(id, x, y);
  }

  /** Retire toutes les images de la fen�tre. */
  public static void retirerTout() {
    panel.effacer();
  }
  
  /** Ferme la fen�tre. */
  public static void fermer() {
    retirerTout();
    frame.dispose();
    panel = null;
    frame = null;
  }

  /** Interrompt le programme pendant un certain temps.
   * @param secondes Nombre de secondes de l'interruption
   */
  public static void pause(double secondes) {
    try {
      Thread.sleep((int) (1000 * secondes));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
  
  @SuppressWarnings("deprecation")
public static void son(String son) {
	  File fichier = new File(son);
	  AudioClip clip = null;
	  try {
		  clip = Applet.newAudioClip(fichier.toURL());
		  clip.play();
	  }
	  catch (MalformedURLException e) {
		  System.out.println(e.getMessage());
	  }
  }
}
