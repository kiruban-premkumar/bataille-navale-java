package modeleBateau;

import exception.BateauCibleException;
import geometrie.Coord;
import gui.GUITools;

import java.util.ArrayList;

import ocean.*;


public class BateauCible implements Bateau {
	
	protected final int id;
	protected final String nom;
	protected int capaciteDeResistance;
	protected final int vie;
	protected Coord coordonnees;
	protected boolean allee;
	
	public BateauCible(int id, String nom, int vie, Coord c) throws BateauCibleException {
		if (vie < 0 || c.getLigne() < 0 || c.getColonne() < 0)
			throw new BateauCibleException(vie,c.getLigne(),c.getColonne());
		else {
			this.id = id;
			this.nom = nom;
			this.capaciteDeResistance = vie;
			this.vie = vie;
			this.coordonnees = c;
			this.allee = true;
		}
	}

	public String getNom() {
		return nom;
	}

	public void setCapacite() {
		assert(capaciteDeResistance >= 0);
		capaciteDeResistance -= 1;
		
	}

	public void restoreVie() {
		capaciteDeResistance = vie;
	}

	public boolean estBateauHopital() {
		return false;
	}

	public Coord getCoordonnee() {
		return coordonnees;
	}

	public void action(ArrayList<Bateau> listeBateaux) {
		assert(canAttack());
	}

	public void seDeplacer(Ocean ocean) {
		Case[][] cellule = ocean.getMatrice();
		int numL = coordonnees.getLigne(); 
		int numC = coordonnees.getColonne();
		
		cellule[numL][numC].removeBateau(this);	
		
		changeCoord(ocean);
		
		numL = coordonnees.getLigne(); 
		numC = coordonnees.getColonne();
		cellule[numL][numC].addBateau(this);
		
		restore(ocean);
	}

	public int getCapacite() {
		return capaciteDeResistance;
	}

	public boolean canAttack() {
		return false;
	}

	public int getVie() {
		return vie;
	}

	public void changeCoord(Ocean ocean) {
		if (coordonnees.getLigne() == ocean.getLigne()-1 && coordonnees.getColonne() == ocean.getColonne()-1)
			allee = false;
		else if (coordonnees.getLigne() == 0 && coordonnees.getColonne() == 0)
			allee = true;
		
		if (allee) {
			if (estSurUneLignePaire()) {
				if(coordonnees.getColonne() == ocean.getColonne()-1)
					coordonnees = new Coord(coordonnees.getLigne()+1,coordonnees.getColonne());
				else
					coordonnees = new Coord(coordonnees.getLigne(),coordonnees.getColonne()+1);
			}
			else if(!estSurUneLignePaire()) {
				if(coordonnees.getColonne() == 0) 
					coordonnees = new Coord(coordonnees.getLigne()+1,coordonnees.getColonne());
				else
					coordonnees = new Coord(coordonnees.getLigne(),coordonnees.getColonne()-1);
			}
		}
		else {
			if (estSurUneLignePaire()) {
				if(coordonnees.getColonne() == 0)
					coordonnees = new Coord(coordonnees.getLigne()-1,coordonnees.getColonne());
				else
					coordonnees = new Coord(coordonnees.getLigne(),coordonnees.getColonne()-1);
			}
			else if(!estSurUneLignePaire()) {
				if(coordonnees.getColonne() ==  ocean.getColonne()-1) 
					coordonnees = new Coord(coordonnees.getLigne()-1,coordonnees.getColonne());
				else
					coordonnees = new Coord(coordonnees.getLigne(),coordonnees.getColonne()+1);
			}
		}
	}

	public void restore(Ocean ocean) {
		for(int i = 0; i < ocean.getListeBateaux().size(); i++) {
			if (ocean.getListeBateaux().get(i).getCoordonnee().compareTo(coordonnees) && ocean.getListeBateaux().get(i).estBateauHopital() 
					&& capaciteDeResistance < vie ) {
				GUITools.son("Son/soin.wav");
				GUITools.ajouterImage(100, "Images/soin.gif", coordonnees.getColonne()*90,
						coordonnees.getLigne()*70);
				GUITools.pause(0.22);
				GUITools.retirerImage(100);
				restoreVie();
			}
		}
	}

	public boolean estSurUneLignePaire() {
		return (coordonnees.getLigne() % 2 == 0);
	}

	public int getId() {
		return id;
	}

}
