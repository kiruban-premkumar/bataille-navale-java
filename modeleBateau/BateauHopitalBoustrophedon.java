package modeleBateau;

import exception.BateauHopitalException;
import ocean.Ocean;
import geometrie.Coord;
import geometrie.Direction;

public class BateauHopitalBoustrophedon extends BateauHopital {
	
	protected boolean allee;
	
	public BateauHopitalBoustrophedon(int id, String nom, Coord c, Direction cap) throws BateauHopitalException {
		super(id, nom, c, cap);
		this.allee = false;
	}
	
	public void changeCoord(Ocean ocean) {
		if (coordonnees.getLigne() == ocean.getLigne()-1 && coordonnees.getColonne() == ocean.getColonne()-1)
			allee = false;
		else if (coordonnees.getLigne() == 0 && coordonnees.getColonne() == 0)
			allee = true;
		
		if (allee == true) {
			if (estSurUneLignePaire()) {
				if(coordonnees.getColonne() == ocean.getColonne()-1)
					coordonnees = new Coord(coordonnees.getLigne()+1,coordonnees.getColonne());
				else
					coordonnees = new Coord(coordonnees.getLigne(),coordonnees.getColonne()+1);
			}
			else if(!estSurUneLignePaire()) {
				if(coordonnees.getColonne() == 0) 
					coordonnees = new Coord(coordonnees.getLigne()+1,coordonnees.getColonne());
				else
					coordonnees = new Coord(coordonnees.getLigne(),coordonnees.getColonne()-1);
			}
		}
		else {
			if (estSurUneLignePaire()) {
				if(coordonnees.getColonne() == 0)
					coordonnees = new Coord(coordonnees.getLigne()-1,coordonnees.getColonne());
				else
					coordonnees = new Coord(coordonnees.getLigne(),coordonnees.getColonne()-1);
			}
			else if(!estSurUneLignePaire()) {
				if(coordonnees.getColonne() ==  ocean.getColonne()-1) 
					coordonnees = new Coord(coordonnees.getLigne()-1,coordonnees.getColonne());
				else
					coordonnees = new Coord(coordonnees.getLigne(),coordonnees.getColonne()+1);
			}
		}
	}
	
	public boolean estSurUneLignePaire() {
		return (coordonnees.getLigne() % 2 == 0);
	}

}
