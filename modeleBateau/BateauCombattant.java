package modeleBateau;

import exception.BateauCombattantException;
import geometrie.Coord;
import gui.GUITools;

import java.util.ArrayList;

import ocean.*;

import appli.AppliBataille;


public class BateauCombattant implements Bateau {
	
	protected final int id;
	protected final String nom;
	protected int capaciteDeResistance;
	protected final int vie;
	protected final int porteeDeTir;
	protected Coord coordonnees;

	public BateauCombattant (int id, String nom, int vie, int portee, Coord c) throws BateauCombattantException {
		if (vie < 0 || portee < 0 || c.getLigne() < 0 || c.getColonne() < 0)
			throw new BateauCombattantException(vie,portee,c.getLigne(),c.getColonne());
		else {
			this.id = id;
			this.nom = nom;
			this.capaciteDeResistance = vie;
			this.vie = vie;
			this.porteeDeTir = portee;
			this.coordonnees = c;
		}
	}
	
	public void action(ArrayList<Bateau> listeBateaux) {
		assert(canAttack());
		Bateau b = this;
		for (int i = 0; i < listeBateaux.size(); i++) {
			if (estAPortee(listeBateaux.get(i)) && !listeBateaux.get(i).estBateauHopital() && 
					!coordonnees.compareTo((listeBateaux.get(i).getCoordonnee()))) {
				b = listeBateaux.get(i);
			}
		}
		if (b != this) {
			b.setCapacite();
			GUITools.son("Son/canon.wav");
			GUITools.ajouterImage(99, "Images/canon.gif", coordonnees.getColonne()*90,
		    		coordonnees.getLigne()*70);
		    AppliBataille.deplacerBateau(coordonnees.getLigne(), 
		    		b.getCoordonnee().getLigne(), coordonnees.getColonne(), 
		    		b.getCoordonnee().getColonne(), 99);
		    GUITools.retirerImage(99);
		}
	}

	public void seDeplacer(Ocean ocean) {
		Case[][] cellule = ocean.getMatrice();
		int numL = coordonnees.getLigne(); 
		int numC = coordonnees.getColonne();
		
		cellule[numL][numC].removeBateau(this);
		
		changeCoord(ocean);
		
		numL = coordonnees.getLigne(); 
		numC = coordonnees.getColonne();
		cellule[numL][numC].addBateau(this);
		
		restore(ocean);
	}
	
	public void setCapacite() {
		assert(capaciteDeResistance >= 0);
		capaciteDeResistance -= 1;
	}

	public void restoreVie() {
		capaciteDeResistance = vie;
	}

	public boolean estBateauHopital() {
		return false;
	}

	public Coord getCoordonnee() {
		return coordonnees;
	}

	public String getNom() {
		return nom;
	}

	public int getCapacite() {
		return capaciteDeResistance;
	}

	public boolean canAttack() {
		return true;
	}

	public int getVie() {
		return vie;
	}

	public void changeCoord(Ocean ocean) {
		Coord j = coordonnees;
		int x,y,a,b ;
		
		for (int i = 0; i < ocean.getListeBateaux().size(); i++) {
			if (this != ocean.getListeBateaux().get(i)) {
				x = Math.abs(ocean.getListeBateaux().get(i).getCoordonnee().getLigne() - coordonnees.getLigne());
				y = Math.abs(ocean.getListeBateaux().get(i).getCoordonnee().getColonne() - coordonnees.getColonne());
				j = ocean.getListeBateaux().get(i).getCoordonnee();
				for (int k = i+1; k < ocean.getListeBateaux().size(); k++) {
					if (this != ocean.getListeBateaux().get(i) && !ocean.getListeBateaux().get(i).estBateauHopital()) {
						a = Math.abs(ocean.getListeBateaux().get(k).getCoordonnee().getLigne() - coordonnees.getLigne());
						b = Math.abs(ocean.getListeBateaux().get(k).getCoordonnee().getColonne() - coordonnees.getColonne());
							if(a < x || b < y)
								j = ocean.getListeBateaux().get(k).getCoordonnee();
					}
				}
			}
		}
		
		if (j.getLigne() < coordonnees.getLigne()) {
			if (j.getColonne() < coordonnees.getColonne())
				coordonnees = new Coord(coordonnees.getLigne()-1, coordonnees.getColonne()-1);
			else if (j.getColonne() == coordonnees.getColonne())
				coordonnees = new Coord(coordonnees.getLigne()-1, coordonnees.getColonne());
			else
				coordonnees = new Coord(coordonnees.getLigne()-1, coordonnees.getColonne()+1);
		}
		else if (j.getLigne() > coordonnees.getLigne()) {
			if (j.getColonne() < coordonnees.getColonne())
				coordonnees = new Coord(coordonnees.getLigne()+1, coordonnees.getColonne()-1);
			else if (j.getColonne() == coordonnees.getColonne())
				coordonnees = new Coord(coordonnees.getLigne()+1, coordonnees.getColonne());
			else
				coordonnees = new Coord(coordonnees.getLigne()+1, coordonnees.getColonne()+1);
		}
		else {
			if (j.getColonne() < coordonnees.getColonne())
				coordonnees = new Coord(coordonnees.getLigne(), coordonnees.getColonne()-1);
			else if (j.getColonne() > coordonnees.getColonne())
				coordonnees = new Coord(coordonnees.getLigne(), coordonnees.getColonne()+1);
		}	
	}

	public void restore(Ocean ocean) {
		for(int i = 0; i < ocean.getListeBateaux().size(); i++) {
			if (ocean.getListeBateaux().get(i).getCoordonnee().compareTo(coordonnees) && ocean.getListeBateaux().get(i).estBateauHopital() 
					&& capaciteDeResistance < vie ) {
				GUITools.son("Son/soin.wav");
				GUITools.ajouterImage(100, "Images/soin.gif", coordonnees.getColonne()*90,
						coordonnees.getLigne()*70);
				GUITools.pause(0.22);
				GUITools.retirerImage(100);
				restoreVie();
			}
		}
	}

	public int getId() {
		return id;
	}
	
	public boolean estAPortee(Bateau bateau) {
		Coord c = bateau.getCoordonnee();
		if ((c.getLigne() >= coordonnees.getLigne() - porteeDeTir) && (c.getLigne() <= coordonnees.getLigne() + porteeDeTir)) {
			if ((c.getColonne() >= coordonnees.getColonne() - porteeDeTir) && (c.getColonne() <= coordonnees.getColonne() + porteeDeTir))
				return true;
		}
		return false;
	}
	
}
