package modeleBateau;

import exception.BateauCombattantException;
import geometrie.Coord;
import gui.GUITools;

import java.util.ArrayList;

import ocean.Bateau;
import ocean.Case;
import ocean.Ocean;


public class BateauCombattantPirate extends BateauCombattant {
	
	protected int tresor;

	public BateauCombattantPirate(int id, String nom, int vie, int portee, Coord c) throws BateauCombattantException {
		super(id, nom, vie, portee, c);
		this.tresor = 0;
	}
	
	public void seDeplacer(Ocean ocean) {
		Case[][] cellule = ocean.getMatrice();
		int numL = getCoordonnee().getLigne(); 
		int numC = getCoordonnee().getColonne();
		
		cellule[numL][numC].removeBateau(this);
		
		changeCoord(ocean);
		if(canSteal(ocean.getListeBateaux())) {
			tresor+=1;
			GUITools.son("Son/tresor.wav");
			GUITools.ajouterImage(101, "Images/tresor.png", coordonnees.getColonne()*90,
					coordonnees.getLigne()*70);
			GUITools.pause(1);
			GUITools.retirerImage(101);
		}

		numL = getCoordonnee().getLigne(); 
		numC = getCoordonnee().getColonne();
		cellule[numL][numC].addBateau(this);
	}

	public boolean canSteal(ArrayList<Bateau> listeBateaux) {
		for(int i = 0; i < listeBateaux.size(); i++) {
			if (listeBateaux.get(i).getCoordonnee().compareTo(coordonnees) && listeBateaux.get(i) != this) {
				return true;
			}
		}
		return false;
	}
	
}
