package modeleBateau;

import exception.BateauCibleException;
import exception.BateauCibleResistantException;
import geometrie.Coord;
import geometrie.Direction;

import java.util.Random;

import ocean.Ocean;


public class BateauCibleResistant extends BateauCible {
	
	protected Direction cap;

	public BateauCibleResistant(int id, String nom, int vie, Coord c, Direction cap) throws BateauCibleException, BateauCibleResistantException {
		super(id, nom, vie, c);
		if (cap == null)
			throw new BateauCibleResistantException();
		else
			this.cap = cap;
	}
	
	public void setCapacite() {
		assert(capaciteDeResistance >= 0);
		Random r = new Random();
		int choix = r.nextInt(9) % 2;
		
		switch(choix) {
		case 0 :
			capaciteDeResistance -= 1;
		break;
		case 1 :
		}
		
	}
	
	public void changeCoord(Ocean ocean) {
		if ((ocean.getLigne() - 1 == coordonnees.getLigne()) || (coordonnees.getLigne() == 0)) {
			if ((ocean.getColonne() - 1 == coordonnees.getColonne()) || (coordonnees.getColonne() == 0))
					cap = cap.inverser();
		}
		else
			cap = cap.deriver(1);
		
		if (coordonnees.getLigne() == 0 && cap.getDx() < 0)
			cap.setDx();
		else if (coordonnees.getLigne() == ocean.getLigne()-1 && cap.getDx() > 0)
			cap.setDx();
		if (coordonnees.getColonne() == 0 && cap.getDy() < 0)
			cap.setDy();
		else if (coordonnees.getColonne() == ocean.getColonne()-1 && cap.getDy() > 0)
			cap.setDy();
		
	    coordonnees = new Coord(coordonnees.getLigne() + cap.getDx(), coordonnees.getColonne() + cap.getDy());
	}

}
