package modeleBateau;

import exception.BateauHopitalException;
import geometrie.*;

import java.util.ArrayList;

import ocean.*;


public class BateauHopital implements Bateau {
	
	protected final int id;
	protected final String nom;
	protected Coord coordonnees;
	protected Direction cap;
	
	public BateauHopital(int id, String nom, Coord c, Direction cap) throws BateauHopitalException {
		if (c.getLigne() < 0 || c.getColonne() < 0 || cap == null)
			throw new BateauHopitalException(c.getLigne(),c.getColonne(),cap);
		else {
			this.id = id;
			this.nom = nom;
			this.coordonnees = c;
			this.cap = cap;
		}
	}

	public void setCapacite() {
		// System.out.println("Impossible d'effectuer cette action.");
	}

	public void restoreVie() {
		// System.out.println(getNom() + " soigne les bateaux.");
	}

	public boolean estBateauHopital() {
		return true;
	}

	public Coord getCoordonnee() {
		return coordonnees;
	}

	public void action(ArrayList<Bateau> ListeBateaux) {
		assert(canAttack());
	}

	public void seDeplacer(Ocean ocean) {
		Case[][] cellule = ocean.getMatrice();
		int numL = coordonnees.getLigne(); 
		int numC = coordonnees.getColonne();
		
		cellule[numL][numC].removeBateau(this);	
		
		changeCoord(ocean);
	    
		numL = coordonnees.getLigne(); 
		numC = coordonnees.getColonne();
		cellule[numL][numC].addBateau(this);
		
		restore(ocean);
	}

	public String getNom() {
		return nom;
	}

	public int getCapacite() {
		return -1;
	}

	public boolean canAttack() {
		return false;
	}
	
	public void changeCoord(Ocean ocean) {
		if ((ocean.getLigne() - 1 == coordonnees.getLigne()) || (coordonnees.getLigne() == 0)) {
			if ((ocean.getColonne() - 1 == coordonnees.getColonne()) || (coordonnees.getColonne() == 0))
					cap = cap.inverser();
		}
		else
			cap = cap.deriver(1);
		
		if (coordonnees.getLigne() == 0 && cap.getDx() < 0)
			cap.setDx();
		else if (coordonnees.getLigne() == ocean.getLigne()-1 && cap.getDx() > 0)
			cap.setDx();
		if (coordonnees.getColonne() == 0 && cap.getDy() < 0)
			cap.setDy();
		else if (coordonnees.getColonne() == ocean.getColonne()-1 && cap.getDy() > 0)
			cap.setDy();
		
	    coordonnees = new Coord(coordonnees.getLigne() + cap.getDx(), coordonnees.getColonne() + cap.getDy());
	}

	public void restore(Ocean ocean) {
		for(int i = 0; i < ocean.getListeBateaux().size(); i++) {
			if (coordonnees.compareTo(ocean.getListeBateaux().get(i).getCoordonnee()) && !ocean.getListeBateaux().get(i).estBateauHopital() 
					&& ocean.getListeBateaux().get(i).getCapacite() < ocean.getListeBateaux().get(i).getVie()) 
				ocean.getListeBateaux().get(i).restoreVie();
		}
	}

	public int getVie() {
		return -3;
	}

	public int getId() {
		return id;
	}
}
