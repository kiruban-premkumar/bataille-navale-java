package exception;

public class BateauCibleResistantException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public BateauCibleResistantException() {
		System.out.println("Vous essayez d'instancier une classe BateauCibleRésistant ayant aucune direction.");
	}

}
