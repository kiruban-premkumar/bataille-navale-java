package exception;

public class BateauCibleException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public BateauCibleException(int vie, int nbL, int nbC) {
		if (nbL < 0 && nbC < 0)
			System.out.println("Vous essayez d'instancier une classe BateauCible ayant un nombre de colonnes " +
			"et de lignes inf�rieur � 0.");
		else {
			if (nbL < 0) {
				System.out.println("Vous essayez d'instancier une classe BateauCible ayant un nombre de " +
							"lignes inf�rieur � 0.");
			}
			if (nbC < 0) {
				System.out.println("Vous essayez d'instancier une classe BateauCible ayant un nombre de colonnes " +
						"inf�rieur � 0.");
			}
		}
		if (vie < 0)
			System.out.println("Vous essayez d'instancier une classe BateauCible ayant une capacit� de r�sistance " +
			"inf�rieur � 0.");
	}

}
