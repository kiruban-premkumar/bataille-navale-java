package exception;

public class BateauCombattantException extends Exception {

	public BateauCombattantException(int vie, int portee, int nbL, int nbC) {
		if (nbL < 0 && nbC < 0)
			System.out.println("Vous essayez d'instancier une classe BateauCombattant ayant un nombre de colonnes " +
			"et de lignes inf�rieur � 0.");
		else {
			if (nbL < 0) {
				System.out.println("Vous essayez d'instancier une classe BateauCombattant ayant un nombre de " +
							"lignes inf�rieur � 0.");
			}
			if (nbC < 0) {
				System.out.println("Vous essayez d'instancier une classe BateauCombattant ayant un nombre de colonnes " +
						"inf�rieur � 0.");
			}
		}
		if (vie < 0)
			System.out.println("Vous essayez d'instancier une classe BateauCombattant ayant une capacit� de r�sistance " +
			"inf�rieur � 0.");
		if (portee < 0)
			System.out.println("Vous essayez d'instancier une classe BateauCombattant ayant une port�e de tir " +
			"inf�rieur � 0.");
	}

	private static final long serialVersionUID = 1L;

}
