package exception;

public class BateauException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public BateauException() {
		System.out.println("Vous essayez d'instancier une classe Bateau dont les coordonées ne sont pas" +
				"compris dans la matrice de la classe Ocean.");
	}

}
