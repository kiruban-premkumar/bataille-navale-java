package exception;

import geometrie.Direction;

public class BateauHopitalException extends Exception {

	private static final long serialVersionUID = 1L;

	public BateauHopitalException(int nbL, int nbC, Direction cap) {
		if (nbL < 0 && nbC < 0)
			System.out.println("Vous essayez d'instancier une classe BateauHopital ayant un nombre de colonnes " +
			"et de lignes inf�rieur � 0.");
		else {
			if (nbL < 0) {
				System.out.println("Vous essayez d'instancier une classe BateauHopital ayant un nombre de " +
							"lignes inf�rieur � 0.");
			}
			if (nbC < 0) {
				System.out.println("Vous essayez d'instancier une classe BateauHopital ayant un nombre de colonnes " +
						"inf�rieur � 0.");
			}
		}
		if (cap == null)
			System.out.println("Vous essayez d'instancier une classe BateauHopital ayant aucune direction.");
	}

}
