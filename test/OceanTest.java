package test;

import static org.junit.Assert.*;
import exception.*;
import geometrie.Coord;
import geometrie.Direction;

import modeleBateau.*;
import ocean.*;

import org.junit.Test;

public class OceanTest {

	private Ocean ocean;
	
	@Test
	public void testOcean() throws OceanException, BateauException, BateauCombattantException,
	BateauCibleException, BateauCibleResistantException, BateauHopitalException {
		ocean = new Ocean(9,7);
		ocean.setBateau(new BateauCombattant(0,"Zodiaque",6,2,new Coord(5,3)));
		ocean.setBateau(new BateauCombattant(1,"Leviathan",6,3,new Coord(0,6)));
		ocean.setBateau(new BateauCombattant(2,"Angel",8,1,new Coord(0,0)));
		ocean.setBateau(new BateauCombattantPirate(3,"Merry",6,2,new Coord(2,1)));
		ocean.setBateau(new BateauCombattantPirate(4,"Black Pearl",5,4,new Coord(5,3)));
		ocean.setBateau(new BateauCible(5,"Barque",10,new Coord(8,6)));
		ocean.setBateau(new BateauCibleResistant(6,"Rider",7,new Coord(1,3), Direction.SUD_EST));
		ocean.setBateau(new BateauHopital(7,"Sainte Croix",new Coord(8,0), Direction.NORD_EST));
		ocean.setBateau(new BateauHopitalBoustrophedon(8,"Celsius",new Coord(1,5), Direction.EST));
		
		for (int i = 0; i < ocean.getListeBateaux().size(); i++) {
			for (int j = i+1; j < ocean.getListeBateaux().size(); j++) {
				assertFalse(ocean.getListeBateaux().get(i).getNom().equals
						(ocean.getListeBateaux().get(j).getNom()));
				assertFalse(ocean.getListeBateaux().get(i).getId() == 
					ocean.getListeBateaux().get(j).getId());
			}
		}
	}

}
