package test;

import static org.junit.Assert.*;
import exception.BateauCibleException;
import exception.BateauCombattantException;
import exception.BateauHopitalException;
import exception.OceanException;
import geometrie.Coord;
import geometrie.Direction;

import modeleBateau.*;
import ocean.*;

import org.junit.Test;

public class BateauTest {

	@Test
	public void testSeDeplacer() throws BateauCibleException, OceanException {
		Ocean ocean = new Ocean(9,7);
		Bateau a = new BateauCible(0,"Barque",10,new Coord(8,6));
		int numL = a.getCoordonnee().getLigne();
		int numC = a.getCoordonnee().getColonne();
		a.seDeplacer(ocean);
		assertTrue(numL != a.getCoordonnee().getLigne() || numC != a.getCoordonnee().getColonne());
	}

	@Test
	public void testSetCapacite() throws BateauCombattantException {
		Bateau a = new BateauCombattantPirate(0,"Black Pearl",5,4,new Coord(5,3));
		int pv = a.getCapacite();
		a.setCapacite();
		assertEquals(a.getCapacite(),pv-1);
	}

	@Test
	public void testRestoreVie() throws BateauCombattantException {
		Bateau a = new BateauCombattant(0,"Angel",8,1,new Coord(0,0));
		a.restoreVie();
		assertTrue(a.getCapacite() == a.getVie());
	}

	@Test
	public void testEstBateauHopital() throws BateauCombattantException, BateauCibleException {
		Bateau a = new BateauCombattant(0,"Angel",8,1,new Coord(0,0));
		Bateau b = new BateauCible(1,"Barque",10,new Coord(8,6));
		assertTrue(!a.estBateauHopital() && !b.estBateauHopital());
	}

	@Test
	public void testCanAttack() throws BateauHopitalException, BateauCibleException {
		Bateau a = new BateauHopital(0,"Sainte Croix",new Coord(8,0), Direction.NORD_EST);
		Bateau b = new BateauCible(1,"Barque",10,new Coord(8,6));
		assertFalse(a.canAttack() && b.canAttack());
	}

}
