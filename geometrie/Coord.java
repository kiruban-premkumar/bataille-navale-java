package geometrie;

public class Coord {
	
	private int numL, numC;
	
	public Coord(int ligne, int colonne) {
	    super();
	    this.numL = ligne;
	    this.numC = colonne;
	}
	
	public int getLigne() {
	    return numL;
	}
	
	public int getColonne() {
	    return numC;
	}
	
	public boolean compareTo(Coord c) {
		return (numL == c.getLigne() && numC == c.getColonne());
	}
}
