package appli;

import exception.BateauCibleException;
import exception.BateauCibleResistantException;
import exception.BateauCombattantException;
import exception.BateauException;
import exception.BateauHopitalException;
import exception.OceanException;
import geometrie.*;
import gui.GUITools;

import java.util.ArrayList;
import java.util.Random;

import ocean.*;

import modeleBateau.*;


public class AppliBataille {

	private static final int LIGNE = 9;
	private static final int COLONNE = 7;
	private static final int RAND_MAX = 499;
	
	public static void main(String[] args)
	throws OceanException, BateauCibleException, BateauCombattantException, 
	BateauHopitalException, BateauCibleResistantException, BateauException {
		
		//
		GUITools.ouvrir("B A T A I L L E  N A V A L E", 700, 700);
		
		int choix, g = 0;
		Ocean ocean = new Ocean(LIGNE,COLONNE);
		
		//
		ArrayList<Bateau> listeBateaux = initialiser(ocean);
		
		// Debut de la partie
		while (ocean.getBateaux() == -1) {
			GUITools.son("Son/mouettes.wav");
			System.out.println("Pas" + g);
			for (int j = 0; j < listeBateaux.size(); j++) {
				if (listeBateaux.get(j).canAttack()) {
					Random r = new Random();
					choix = r.nextInt(RAND_MAX) % 2;
					switch (choix) {
					case 0 :
						listeBateaux.get(j).action(ocean.getListeBateaux());
						for (int i = 0; i < listeBateaux.size(); i++) {
							if (ocean.autodestruction(i)) {
								enleverBateaux(listeBateaux, i);
								ocean.destruction(i);
							}
						}
						break;
					case 1 :
						int numL = listeBateaux.get(j).getCoordonnee().getLigne();
						int numC = listeBateaux.get(j).getCoordonnee().getColonne();
						listeBateaux.get(j).seDeplacer(ocean);
						deplacerBateau(numL, listeBateaux.get(j).getCoordonnee().getLigne(),
								numC, listeBateaux.get(j).getCoordonnee().getColonne(),
								listeBateaux.get(j).getId());
						break;
					}
				}
				else {
					int numL = listeBateaux.get(j).getCoordonnee().getLigne();
					int numC = listeBateaux.get(j).getCoordonnee().getColonne();
					listeBateaux.get(j).seDeplacer(ocean);
					listeBateaux = ocean.getListeBateaux();
					deplacerBateau(numL, listeBateaux.get(j).getCoordonnee().getLigne(),
							numC, listeBateaux.get(j).getCoordonnee().getColonne(),
							listeBateaux.get(j).getId());
				}
			}
			if (ocean.getBateaux() != -1) {
				System.out.println("\n--------------------------- FIN DE PARTIE ---------------------------- \n");
				System.out.println(ocean.getListeBateaux().get(ocean.getBateaux()).getNom() + " a gagne la partie.");
			}
			g++;
		}
		GUITools.retirerTout();
		GUITools.fermer();
		
		System.out.println("Pour plus de detail, veuillez consulter les fichiers " +
				"dans le repertoire 'Dossier'.\nVous trouverez le programme detaille " +
				"par numero de pas de simulation.\n\nProjet de TRACY NGOT & KIRUBAN PREMKUMAR (GROUPE 214).");
	}

	public static void deplacerBateau(int x0, int x1, int y0, int y1, int id) {
		x0 = x0 * 70; x1 = x1 * 70; y0 = y0 * 90; y1 = y1 * 90;
		
		int a = x0 - x1;
		int b = y0 - y1;
		while (x0 != x1 || y0 != y1) {
			GUITools.pause(0.005);
			if (a > 0) { a--;
				if (b > 0) { b--; GUITools.placerImage(id, y0--, x0--); }
				else if (b < 0) { b++; GUITools.placerImage(id, y0++, x0--); }
				else GUITools.placerImage(id, y0, x0--); 
			}
			else if (a < 0) { a++;
				if (b > 0) { b--; GUITools.placerImage(id, y0--, x0++); }
				else if (b < 0) { b++; GUITools.placerImage(id, y0++, x0++); }
				else GUITools.placerImage(id, y0, x0++);
			} 
			else {
				if (b > 0) { b--; GUITools.placerImage(id, y0--, x0); }
				else if (b < 0) { b++; GUITools.placerImage(id, y0++, x0); }
			}
		}
	}

	public static void enleverBateaux(ArrayList<Bateau> listeBateaux, int i) {
		GUITools.ajouterImage(102, "Images/destruction.gif", listeBateaux.get(i).getCoordonnee().getColonne()*90,
				listeBateaux.get(i).getCoordonnee().getLigne()*70);
		GUITools.son("Son/explosion.wav");
		deplacerBateau(listeBateaux.get(i).getCoordonnee().getLigne(), 
				listeBateaux.get(i).getCoordonnee().getLigne()+1, 
				listeBateaux.get(i).getCoordonnee().getColonne(), 
				listeBateaux.get(i).getCoordonnee().getColonne()+1, 102);
		GUITools.retirerImage(102);
		GUITools.retirerImage(listeBateaux.get(i).getId());
	}

	public static void placerBateaux(ArrayList<Bateau> listeBateaux) {
		ArrayList<String> Images = new ArrayList<String>();
		Images.add("Images/bateau0.png");
		Images.add("Images/bateau1.png");
		Images.add("Images/bateau2.png");
		Images.add("Images/bateau3.png");
		Images.add("Images/bateau4.png");
		Images.add("Images/bateau5.png");
		Images.add("Images/bateau6.png");
		Images.add("Images/bateau7.png");
		Images.add("Images/bateau8.png");
		Images.add("Images/bateau9.png");
		
		for (int j = 0; j < listeBateaux.size(); j++) {
		    GUITools.ajouterImage(listeBateaux.get(j).getId(), Images.get(j),0,0);
			deplacerBateau(0, listeBateaux.get(j).getCoordonnee().getLigne(),
					0, listeBateaux.get(j).getCoordonnee().getColonne(),
					listeBateaux.get(j).getId());
		}	
	}

	public static ArrayList<Bateau> initialiser(Ocean ocean)  
	throws BateauCibleException, BateauCombattantException, 
	BateauHopitalException, BateauCibleResistantException, BateauException {
		// Insertion des bateaux
		ocean.setBateau(new BateauCombattant(0,"Zodiaque",6,2,new Coord(5,3)));
		ocean.setBateau(new BateauCombattant(1,"Leviathan",6,3,new Coord(0,6)));
		ocean.setBateau(new BateauCombattant(2,"Angel",8,1,new Coord(0,0)));
		ocean.setBateau(new BateauCombattantPirate(3,"Merry",6,2,new Coord(2,1)));
		ocean.setBateau(new BateauCombattantPirate(4,"Black Pearl",5,4,new Coord(5,3)));
		ocean.setBateau(new BateauCible(5,"Barque",10,new Coord(8,6)));
		ocean.setBateau(new BateauCibleResistant(6,"Rider",7,new Coord(1,3), Direction.SUD_EST));
		ocean.setBateau(new BateauHopital(7,"Sainte Croix",new Coord(8,0), Direction.NORD_EST));
		ocean.setBateau(new BateauHopitalBoustrophedon(8,"Celsius",new Coord(1,5), Direction.EST));
		
		placerBateaux(ocean.getListeBateaux());
		
		return ocean.getListeBateaux();
	}
	
}
